package pl.codementors.home_html.Controllers;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.util.Map;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@org.springframework.stereotype.Controller
public class Controller {

    @RequestMapping(value = "/home", method = GET)
    public String redirectTo(
            @RequestParam(value = "redirect", required = false) final String redirect, HttpServletResponse response, HttpSession session) {
        session.setAttribute("redirect", redirect);
        if(redirect!=null && redirect.trim().length()>0) {
            return "redirect:" + redirect;
        }
        response.setStatus(HttpStatus.NOT_FOUND.value());
        return "redirect:error/error.html";
    }

    @RequestMapping(value = "/dynamic")
    public String index(HttpServletRequest request, Map<String, Object> model) {
        model.put("message", request.getParameter("name"));
        return "dynamic";
    }
}
