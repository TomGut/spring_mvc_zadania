package pl.codementors.home_html;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HomeHtmlApplication {

	public static void main(String[] args) {
		SpringApplication.run(HomeHtmlApplication.class, args);
	}

}
