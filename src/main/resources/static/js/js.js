/*
    JSONs
*/
const training =
    {
        "name": "Nazwa",
        "date": "Data",
        "seats": "Miejsca",
        "leadBy": "Prelegent",
        "theme": [
            "Nazwa", "Opis"
        ],
    };

const trainings = 
    {
        "name": [
            "szkolenie 1", "szkolenie 2", "szkolenie 3"
        ],
        "date": [
            "2019-04-01", "2019-04-02", "2019-04-03"
        ],
        "seats": [
            200, 20, 30
        ],
        "leadBy": [
            "Prelegent1", "Prelegent2", "Prelegent3"
        ],
        "theme": [
            ["Szkolonko1", "Opis szkolonko1"],
            ["Szkolonko2", "Opis szkolonko2"],
            ["Szkolonko3", "Opis szkolonko3"]
        ]
    }

/*
    List.html layout
*/
function createListLayout(){
    //function createHtmlElWithText(elType, newElId, newElClazz, toAttachId, text)
     //function createHtmlEl(elType, newElId, newElClazz, toAttachId)
    createHtmlEl("main", "main_list", "main_list", "body");
    createHtmlElWithText("h2", "main_list-h2", "main_list-h2", "main_list", "Opis Szkoleń");
    createHtmlEl("aside", "aside_list", "aside_list", "main_list");
    createHtmlEl("ul", "aside_list-ul", "aside_list-ul", "aside_list");
}

/*
    Tables
*/
function createTables(){
    //main table components
    createTable("table", "main_list");
    createTableHead("table-thead", "table");
    createTableTr("table-thead", "table-thead_header");
    createTableBody("table-tbody", "table");
    //main table header text
    createTableTh("table-thead_header", "thead_header-name", training.name);
    createTableTh("table-thead_header", "thead_header-date", training.date);
    createTableTh("table-thead_header", "thead_header-seats", training.seats);
    createTableTh("table-thead_header", "thead_header-leadBy", training.leadBy);
    createTableTh("table-thead_header", "thead_header-info", "Info");
   
    for(i=0; i<trainings.name.length; i++){
        //main table
        createTableTr("table-tbody", "mainTbody-tr" + i);
        createTableTd(("mainTbody-tr" + i + "--name" + i), "mainTbody-tr" + i , trainings.name[i]);
        createTableTd(("mainTbody-tr" + i + "--date" + i), "mainTbody-tr" + i , trainings.date[i]);
        createTableTd(("mainTbody-tr" + i + "--seats" + i), "mainTbody-tr" + i , trainings.seats[i]);
        createTableTd(("mainTbody-tr" + i + "--leadBy" + i), "mainTbody-tr" + i , trainings.leadBy[i]);
        //nested tables
        createTable("nested_table" + i, "mainTbody-tr" + i);
        createTableTr("nested_table" + i, "nested-tr" + i);
        createTableTd("td", "nested-tr" + i, trainings.theme[i][0]);
        createTableTd("td", "nested-tr" + i, trainings.theme[i][1]);
        //buttons in table
        createButton("button-add--tr" + i, "nested-tr" + i, "ADD", buttonAdd);
        createButton("button-del--tr" + i, "nested-tr" + i, "DEL", buttonDel);
    }
}

//specific HTML elements creation
function createHtmlEl(elType, newElId, newElClazz, toAttachId){
    const newEl = document.createElement(elType);
    newEl.id = newElId;
    newEl.classList.add(newElClazz);
    const toAttachTo = document.getElementById(toAttachId);
    toAttachTo.appendChild(newEl);
}

function createHtmlElWithText(elType, newElId, newElClazz, toAttachId, text){
    const newEl = document.createElement(elType);
    newEl.id = newElId;
    newEl.classList.add(newElClazz);
    newEl.innerText = text;
    const toAttachTo = document.getElementById(toAttachId);
    toAttachTo.appendChild(newEl);
}

function createButton(buttonClazz, toAttachId, text, func){
    const button = document.createElement("button");
    button.innerText = text;
    button.id = buttonClazz;
    button.classList.add(buttonClazz);
    button.onclick = func;
    const toAttachTo = document.getElementById(toAttachId);
    toAttachTo.appendChild(button);
}

//onclick funcs for buttons
function buttonAdd(){
    const button = document.getElementById(this.id);
    const tr = document.getElementById(button.parentElement.parentElement.parentElement.id);
    const nameTd = document.getElementById(tr.firstChild.id);
    const nameInnerText = document.getElementById(nameTd.id).innerText;
    const ul = document.getElementById("aside_list-ul");
    const li = document.createElement("li");
    li.id = nameTd.id + "--li";
    li.classList.add(nameTd.id + "--li");
    li.innerText = nameInnerText;
    ul.appendChild(li);
}

function buttonDel(){
    const button = document.getElementById(this.id);
    const tr = document.getElementById(button.parentElement.parentElement.parentElement.id);
    const nameTd = document.getElementById(tr.firstChild.id);
    const ul = document.getElementById("aside_list-ul");
    const li = document.getElementById(nameTd.id + "--li");
    ul.removeChild(li);
}

//table components
function createTable(tableClazz, toAttachId){
    const toAttachTo = document.getElementById(toAttachId);
    const table = document.createElement("table");
    table.classList.add(tableClazz);
    table.id = tableClazz;
    toAttachTo.appendChild(table);
}

function createTableHead(headClazz, toAttachId){
    const toAttachTo = document.getElementById(toAttachId);
    const thead = document.createElement("thead");
    thead.id = headClazz;
    thead.classList.add(headClazz);
    toAttachTo.appendChild(thead);
}

function createTableBody(bodyClazz, toAttachId){
    const toAttachTo = document.getElementById(toAttachId);
    const tbody = document.createElement("tbody");
    tbody.id = bodyClazz;
    tbody.classList.add(bodyClazz);
    toAttachTo.appendChild(tbody);
}

function createTableTd(newElementClazz, toAttachId, text){
    const td = document.createElement("td");
    td.innerText = text;
    td.id = newElementClazz;
    td.classList.add(newElementClazz);
    const toAttachTo = document.getElementById(toAttachId);
    toAttachTo.appendChild(td);
}

function createTableTr(toAttachId, newElementClazz){
    const tr = document.createElement("tr");
    tr.id = newElementClazz;
    tr.classList.add(newElementClazz);
    const toAttachTo = document.getElementById(toAttachId);
    toAttachTo.appendChild(tr);
}

function createTableTh(toAttachId, newElementClazz, text){
    const th = document.createElement("th");
    th.id = newElementClazz;
    th.classList.add(newElementClazz);
    th.innerText = text;
    const toAttachTo = document.getElementById(toAttachId);
    toAttachTo.appendChild(th);
}